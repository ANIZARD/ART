

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";



--
-- Base de données : `art`
--

-- --------------------------------------------------------

--
-- Structure de la table `coordonneesannonceurs`
--

CREATE TABLE `coordonneesannonceurs` (
  `id` int(11) NOT NULL COMMENT 'id_annonceur',
  `nom` text NOT NULL,
  `email` text NOT NULL
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------




--
-- Index pour la table `coordonnesannonceurs`
--
ALTER TABLE `coordonneesannonceurs`
  ADD UNIQUE KEY `id` (`id`);



